import numpy as np
import scipy.io
    
filename = '/Users/hafizimtiaz/OneDrive/Backup from Dropbox/Rutgers Research/COINSTAC Learning/py_multishot_NMF/SYNTH_database_sm.mat'
mat = scipy.io.loadmat(filename)
V = np.array(mat['V'])
K = 2
d, n = V.shape
S = 5
num_samples_per_site = n/S

for s in range(S):
    st_idx = 0 + s * num_samples_per_site
    en_idx = num_samples_per_site + s * num_samples_per_site
    X = V[:, st_idx : en_idx]
    H = np.random.random([K,num_samples_per_site])
    R = np.random.random([d,num_samples_per_site])
    filename = "site" + str(s)
    np.savez(filename, X, H, R)
    