import json;
import argparse
from os import listdir
from os.path import isfile, join
import sys
import numpy as np

########### helper functions ###########

def mySoftThresh(x, lam, M):
    # Implementation of the soft thresholding function defined in Renbo
    # Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    #
    # x - input vector of dimension F
    # lam - penalty parameter (here threshold parameter)
    # M - parameter of the constraint set of the outlier r
    # y - output vector of dimension F
    OPT1 = 0
    OPT2 = x - np.sign(x) * lam
    OPT3 = np.sign(x) * M   
    CON1 = np.less(abs(x), lam)
    CON2 = np.logical_and(np.greater_equal(abs(x), lam), np.less_equal(abs(x), lam + M))
    CON3 = np.greater(abs(x), lam + M)    
    y = np.multiply(OPT1, CON1) + np.multiply(OPT2, CON2) + np.multiply(OPT3, CON3)    
    return y
    
def myProjGrad(x, gradx, lLim, uLim):
    OPT1 = gradx
    OPT2 = np.minimum(0, gradx)
    OPT3 = np.maximum(0, gradx)
    CON1 = np.logical_and(np.greater_equal(x, lLim), np.less_equal(x, uLim))
    CON2 = np.less(x, lLim)
    CON3 = np.greater(x, uLim)    
    pgd = np.multiply(OPT1, CON1) + np.multiply(OPT2, CON2) + np.multiply(OPT3, CON3)
    return pgd
    
def myNNLS(V, W, Z, R, lam, iterMin, iterMax, tol):

    WtW = np.dot(W.T, W)
    L = np.linalg.norm(WtW)
    H = Z
    WtV = np.dot(W.T, V-R)
    Grad = np.dot(WtW, H) - WtV
    GradR = R - (V - np.dot(W,H)) + lam * np.sign(R)
    initgrad = np.sqrt(np.linalg.norm(Grad, ord='fro')**2 + np.linalg.norm(GradR, ord='fro')**2);
    alpha1 = 1.0; M = 1;
    
    for itr in range(iterMax):
        # update H
        H0 = H
        H = np.maximum(Z - Grad/L, 0)
        alpha2 = 0.5 * (1.0 + np.sqrt(1.0 + 4 * alpha1**2))
        Z = H + ((alpha1 - 1.0) / alpha2) * (H - H0)
        alpha1 = alpha2
        
        # update R
        R = mySoftThresh(V - np.dot(W, H), lam, 1)
        GradR = R - (V - np.dot(W, H)) + lam * np.sign(R)
        
        WtV = np.dot(W.T, V-R)
        Grad = np.dot(WtW, Z) - WtV;
        
        # Stopping criteria
        if itr >= iterMin:
            # Lin's stopping condition
            projnorm = np.sqrt( np.linalg.norm(myProjGrad(Z, Grad, 0, 1e10), ord='fro')**2 + np.linalg.norm(myProjGrad(R, GradR, -M, M), ord='fro')**2 )
            if projnorm <= tol*initgrad:
                break;
    
    Grad = np.dot(WtW, H) - WtV
    
    return H, R, itr, Grad
    
def NNLS(Z, WtW, WtV, iterMin, iterMax, tol):

    L = np.linalg.norm(WtW)
    H = Z
    Grad = np.dot(WtW, Z) - WtV
    alpha1 = 1.0
    
    for itr in range(iterMax):
        H0 = H
        H = np.maximum(Z - Grad/L, 0)
        alpha2 = 0.5 * (1.0 + np.sqrt(1.0 + 4 * alpha1**2))
        Z = H + ((alpha1 - 1.0) / alpha2) * (H - H0)
        alpha1 = alpha2
        Grad = np.dot(WtW, Z) - WtV;
        
        # Stopping criteria
        if itr >= iterMin:
            # Lin's stopping condition
            pgn = np.linalg.norm(np.multiply(Grad, np.logical_or(np.less(Grad,0), np.greater(Z,0))), ord = 'fro')
            if pgn <= tol:
                break
    
    Grad = np.dot(WtW, H) - WtV
    
    return H, itr, Grad

#####################################################

parser = argparse.ArgumentParser(description='read beta vector from single site and do beta averaging!')
parser.add_argument('--run', type=str,  help='grab coinstac args')
args = parser.parse_args()
args.run = json.loads(args.run)

#inspect what args were passed
#runInputs = json.dumps(args.run, sort_keys=True, indent=4, separators=(',', ': '))
#sys.stderr.write(runInputs + "\n")

#if 'remoteResult' in args.run and \
#    'data' in args.run['remoteResult'] and \
#    username in args.run['remoteResult']['data']:
#    sys.exit(0); # no-op!  we already contributed our data



user_results = args.run['userResults']

#sum_beta_vector = np.zeros(np.shape(user_results[0]['data']['beta_vector']))
n_site = len(user_results)

### set some parameter values ###
K = 2                      # low rank of the synthetic data
tol = 1e-7                 # tolerance level 
maxiter = 1000             # maximum allowed iterations 
timelimit = 1e5            # maximum allowed time for iterations 
MinIter = 10               # minimum number of iterations 

flag = True # flag for initial one iteration
for i in range(0,n_site):
    if user_results[i]['data']['first'] == False:
        flag = False

if flag:
    ### for the initial iteration ###
    initgradW = 0
    initgradH = 0
    initgradR = 0
    objective_old = 0        
    
    for i in range(0,n_site):
        initgradW += np.array(user_results[i]['data']['G'])
        initgradH += user_results[i]['data']['initgradH']
        initgradR += user_results[i]['data']['initgradR']
        objective_old += user_results[i]['data']['objective_old']
        
    initgradW = np.linalg.norm(initgradW, ord='fro')
    initgrad = np.sqrt(initgradW**2 + initgradH + initgradR)
    W = np.array(user_results[0]['data']['W'])
    
    tolW = max(0.001, tol) * initgrad
    tolH = tolW * np.ones([n_site])
    
    # Historical information
    #HIS = {}
    HIS = 0
    #HIS['objf'] = []; HIS['objf'].append(objective_old)
    #HIS['prjg'] = []; HIS['prjg'].append(initgrad)
    
    # send these information to sites
    sys.stderr.write("Initial computation at master : {}".format(initgrad) + "\n")
    send_dict = {'complete': False, 'W': W.tolist(), 'initgrad': initgrad, 'objective_old' : objective_old, 'tolH' : tolH.tolist(), 'HIS' : HIS, 'tolW' : tolW}
    computationOutput = json.dumps(send_dict, sort_keys=True, indent=4, separators=(',', ': '))    
    sys.stdout.write(computationOutput)    
    
else:
### for subsequent iterations ###
    W = np.array(user_results[0]['data']['W'])
    tolW = user_results[0]['data']['tolW']
    tolH = user_results[-1]['data']['tolH']
    HIS = user_results[0]['data']['HIS']
    
    gradW_HHt = 0
    gradW_VHt = 0    
    projgradH = 0
    projgradR = 0
    objective_old = 0
    
    for i in range(0,n_site):
        projgradH += user_results[i]['data']['projgradH']
        projgradR += user_results[i]['data']['projgradR']
        gradW_HHt += np.array(user_results[i]['data']['gradW_HHt'])
        gradW_VHt += np.array(user_results[i]['data']['gradW_VHt'])
        objective_old += user_results[i]['data']['objective_old']

    W, iterW, gradW = NNLS(W.T, gradW_HHt, gradW_VHt.T, MinIter, maxiter, tolW)
    W = W.T
    gradW = gradW.T
    
    tmp1 = np.sqrt(np.diag(np.dot(W.T, W)))
    tmp2 = np.diag(np.divide(1, tmp1))
    W = np.dot(W, tmp2)
    
    if iterW < MinIter:
        tolW = tolW / 10.0
        
    HIS += 1
    tmp = np.multiply(gradW, np.logical_or(np.less(gradW, 0), np.greater(W, 0)))
    projnorm = np.sqrt(np.linalg.norm(tmp, ord='fro')**2 + projgradH + projgradR)
    objf = objective_old
    #HIS['objf'].append(objf)
    #HIS['prjg'].append(projnorm)
    
    # stopping condition check
    if iterW >= MinIter:
        if (projnorm <= tol*initgrad) or (HIS >= maxiter): # iterations converged
            # send these information to sites
            sys.stderr.write("Convergence achieved : {}".format(projnorm / initgrad) + "\n")
            send_dict = {'complete': True, 'W': W.tolist(), 'initgrad': initgrad, 'objective_old' : objective_old, 'projnorm' : projnorm, 'HIS' : HIS}
            computationOutput = json.dumps(send_dict, sort_keys=True, indent=4, separators=(',', ': '))
            sys.stdout.write(computationOutput)
        else:
            sys.stderr.write("Top-level iteration finished : {}".format(HIS) + "\n")
            send_dict = {'complete': False, 'W': W.tolist(), 'initgrad': initgrad, 'objective_old' : objective_old, 'projnorm' : projnorm, 'tolH' : tolH.tolist(), 'HIS' : HIS, 'tolW' : tolW}
            computationOutput = json.dumps(send_dict, sort_keys=True, indent=4, separators=(',', ': '))
            sys.stdout.write(computationOutput)
    
#sys.stderr.write("Done! consensus subspace energy : {}".format(var_true)+"\n")
#computationOutput = json.dumps({'complete': True, 'var_true': var_true}, sort_keys=True, indent=4, separators=(',', ': '))

# preview output data
#sys.stderr.write(computationOutput + "\n")

# send results
sys.stdout.write(computationOutput)