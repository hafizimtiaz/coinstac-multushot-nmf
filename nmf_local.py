import json;
import argparse
from os import listdir
from os.path import isfile, join
import sys
import numpy as np

########### helper functions ###########

def mySoftThresh(x, lam, M):
    # Implementation of the soft thresholding function defined in Renbo
    # Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    #
    # x - input vector of dimension F
    # lam - penalty parameter (here threshold parameter)
    # M - parameter of the constraint set of the outlier r
    # y - output vector of dimension F
    OPT1 = 0
    OPT2 = x - np.sign(x) * lam
    OPT3 = np.sign(x) * M   
    CON1 = np.less(abs(x), lam)
    CON2 = np.logical_and(np.greater_equal(abs(x), lam), np.less_equal(abs(x), lam + M))
    CON3 = np.greater(abs(x), lam + M)    
    y = np.multiply(OPT1, CON1) + np.multiply(OPT2, CON2) + np.multiply(OPT3, CON3)    
    return y
    
def myProjGrad(x, gradx, lLim, uLim):
    OPT1 = gradx
    OPT2 = np.minimum(0, gradx)
    OPT3 = np.maximum(0, gradx)
    CON1 = np.logical_and(np.greater_equal(x, lLim), np.less_equal(x, uLim))
    CON2 = np.less(x, lLim)
    CON3 = np.greater(x, uLim)    
    pgd = np.multiply(OPT1, CON1) + np.multiply(OPT2, CON2) + np.multiply(OPT3, CON3)
    return pgd
    
def myNNLS(V, W, Z, R, lam, iterMin, iterMax, tol):

    WtW = np.dot(W.T, W)
    L = np.linalg.norm(WtW)
    H = Z
    WtV = np.dot(W.T, V-R)
    Grad = np.dot(WtW, H) - WtV
    GradR = R - (V - np.dot(W,H)) + lam * np.sign(R)
    initgrad = np.sqrt(np.linalg.norm(Grad, ord='fro')**2 + np.linalg.norm(GradR, ord='fro')**2);
    alpha1 = 1.0; M = 1;
    
    for itr in range(iterMax):
        # update H
        H0 = H
        H = np.maximum(Z - Grad/L, 0)
        alpha2 = 0.5 * (1.0 + np.sqrt(1.0 + 4 * alpha1**2))
        Z = H + ((alpha1 - 1.0) / alpha2) * (H - H0)
        alpha1 = alpha2
        
        # update R
        R = mySoftThresh(V - np.dot(W, H), lam, 1)
        GradR = R - (V - np.dot(W, H)) + lam * np.sign(R)
        
        WtV = np.dot(W.T, V-R)
        Grad = np.dot(WtW, Z) - WtV;
        
        # Stopping criteria
        if itr >= iterMin:
            # Lin's stopping condition
            projnorm = np.sqrt( np.linalg.norm(myProjGrad(Z, Grad, 0, 1e10), ord='fro')**2 + np.linalg.norm(myProjGrad(R, GradR, -M, M), ord='fro')**2 )
            if projnorm <= tol*initgrad:
                break;
    
    Grad = np.dot(WtW, H) - WtV
    
    return H, R, itr, Grad
    
def NNLS(Z, WtW, WtV, iterMin, iterMax, tol):

    L = np.linalg.norm(WtW)
    H = Z
    Grad = np.dot(WtW, Z) - WtV
    alpha1 = 1.0
    
    for itr in range(iterMax):
        H0 = H
        H = np.maximum(Z - Grad/L, 0)
        alpha2 = 0.5 * (1.0 + np.sqrt(1.0 + 4 * alpha1**2))
        Z = H + ((alpha1 - 1.0) / alpha2) * (H - H0)
        alpha1 = alpha2
        Grad = np.dot(WtW, Z) - WtV;
        
        # Stopping criteria
        if itr >= iterMin:
            # Lin's stopping condition
            pgn = np.linalg.norm(np.multiply(Grad, np.logical_or(np.less(Grad,0), np.greater(Z,0))), ord = 'fro')
            if pgn <= tol:
                break
    
    Grad = np.dot(WtW, H) - WtV
    
    return H, itr, Grad

#####################################################

parser = argparse.ArgumentParser(description='help read in my data from my local machine!')
parser.add_argument('--run', type=str,  help='grab coinstac args')
args = parser.parse_args()
args.run = json.loads(args.run)

username = args.run['username']

# inspect what args were passed
# runInputs = json.dumps(args.run, sort_keys=True, indent=4, separators=(',', ': '))
# sys.stderr.write(runInputs + "\n")

if 'remoteResult' in args.run and \
    'data' in args.run['remoteResult'] and \
    username in args.run['remoteResult']['data']:
    sys.exit(0); # no-op!  we already contributed our data

passedDir = args.run['userData']['dirs'][0]
sys.stderr.write("Reading files from dir: " + passedDir + "\n")

files = [f for f in listdir(passedDir) if isfile(join(passedDir, f))]

### read data from local site ###

for f in files:
    if not(f == '.DS_Store'):
        tmp = np.load(join(passedDir,f))
        X = tmp['arr_0']
        H = tmp['arr_1']
        R = tmp['arr_2']

### set some parameter values ###
d, n = X.shape             # dimension and sample size of local data 
K = 2                      # low rank of the synthetic data
lam = 1 / np.sqrt(d)       # regularizer coefficient for outliers 
tol = 1e-7                 # tolerance level 
maxiter = 1000             # maximum allowed iterations 
timelimit = 1e5            # maximum allowed time for iterations 
MinIter = 10               # minimum number of iterations 
M = 1                      # inf norm of each col of outliers
 
### initialize W or get it from master node ###

if ('data' in args.run['remoteResult']) and ('W' in args.run['remoteResult']['data']): # iteration id > 1
    W = np.array(args.run['remoteResult']['data']['W'])
    tolH = np.array(args.run['remoteResult']['data']['tolH'])
    tolW = np.array(args.run['remoteResult']['data']['tolW'])
    objective_old = np.array(args.run['remoteResult']['data']['objective_old'])
    HIS = np.array(args.run['remoteResult']['data']['HIS'])
    initgrad = np.array(args.run['remoteResult']['data']['initgrad'])
    
    sid = passedDir[-1] # site id is the last digit of the directory name (hack)
    
    # update H and R at local site
    H, R, iterHR, tmp = myNNLS(X, W, H, R, lam, MinIter, maxiter, tolH[sid])
    if iterHR < MinIter:
        tolH[sid] = 0.1 * tolH[sid]
        
    gradH = np.dot(np.dot(W.T, W), H) - np.dot(W.T, X - R)
    gradR = R - (X - np.dot(W, H)) + lam * np.sign(R)
    projgradH = np.linalg.norm(myProjGrad(H, gradH, 0, 1e10), ord='fro')**2    # scalar 
    projgradR = np.linalg.norm(myProjGrad(R, gradR, -M, M), ord='fro')**2      # scalar 
    gradW_HHt = np.dot(H, H.T)                                                 # k-by-k matrix
    gradW_VHt = np.dot(X - R, H.T)                                             # d-by-k matrix
    objective_old = (1.0/2) * np.linalg.norm( X - np.dot(W, H) - R, ord='fro' )**2 + lam * np.sum(abs(R))
    
    # send and store updated values
    send_dict = {'W' : W.tolist(), 'gradW_HHt': gradW_HHt.tolist(), 'gradW_VHt': gradW_VHt.tolist(), 'projgradH' : projgradH, 'projgradR' : projgradR, 'objective_old' : objective_old, 'first' : False, 'tolH' : tolH.tolist(), 'tolW' : tolW, 'HIS' : HIS, 'initgrad' : initgrad}
    computationOutput = json.dumps(send_dict, sort_keys=True, indent=4, separators=(',', ': '))
    sys.stdout.write(computationOutput)
    
    sys.stderr.write("Writing updated values...\n")
    np.savez(join(passedDir,f), X, H, R)
    
else: # first iteration
    W = lam * np.ones([d,K]) # initializing W to a specific matrix
    
    G = np.dot(W, np.dot(H, H.T)) - np.dot(X - R, H.T)
    initgradH = np.linalg.norm( np.dot(np.dot(W.T, W), H) - np.dot(W.T, X - R), ord='fro' )**2
    initgradR = np.linalg.norm( R - (X - np.dot(W, H)) + lam * np.sign(R), ord='fro' )**2
    objective_old = (1.0/2) * np.linalg.norm( X - np.dot(W, H) - R, ord='fro' )**2 + lam * np.sum(abs(R))
    
    # send initial values to master
    send_dict = {'W' : W.tolist(), 'G': G.tolist(), 'initgradH' : initgradH, 'initgradR' : initgradR,'objective_old' : objective_old, 'first' : True}
    computationOutput = json.dumps(send_dict, sort_keys=True, indent=4, separators=(',', ': '))
    sys.stdout.write(computationOutput)
    
#computationOutput = json.dumps({'en': en}, sort_keys=True, indent=4, separators=(',', ': '))

# preview output data
# sys.stderr.write(computationOutput + "\n")

# send results


